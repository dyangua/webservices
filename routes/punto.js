const express = require('express');
const router = express.Router();
const url = require('url');
const db = require('../config/connection');

// http://localhost:3000/punto?lat=4&lon=9&vel=45.40&fecha=20/10/12&hora=08:10&viaje=1&transporte=1&usuario=danniel

// {
//     "lat" : "1",
//     "lon" : "1",
//     "vel" : "1",
//     "fecha" : "26/05/2018",
//     "hora" : "21:45",
//     "viaje" : "1",
//     "transporte" : "1",
//     "usuario" : "Travis"
// }


router.post('/', function(req, res, next) {

  const datos = {
    lat: req.body.lat,
    lon: req.body.lon,
    vel: req.body.vel,
    fecha: req.body.fecha,
    hora: req.body.hora,
    viaje: req.body.viaje,
    transporte: req.body.transporte,
    usuario: req.body.usuario,
  }

  const myQuery = `INSERT INTO datos (da_latitud,da_longitud,da_velocidad,da_fecha,da_hora,da_viaje,unidades_transporte_ut_id,usuario_u_id) values ("${datos.lat}","${datos.lon}","${datos.vel}","${datos.fecha}","${datos.hora}",${datos.viaje},${datos.transporte},${datos.usuario}) `;

  db.query(myQuery, datos, function (err, results, field) {
    if (err) {
      throw err
    }
    res.send("Guardado");
  });
});

module.exports = router;
