const express = require('express');
const router = express.Router();
const db = require('../config/connection');

router.get('/', function(req, res, next) {
  const myQuery = `SELECT * FROM unidades_transporte; `;
  res.header("Access-Control-Allow-Origin", "*"); //security
  db.query(myQuery, function (err, results, field) {
    res.send(results);
  });
});

module.exports = router;
